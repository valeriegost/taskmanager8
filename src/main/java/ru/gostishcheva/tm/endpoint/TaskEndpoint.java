package ru.gostishcheva.tm.endpoint;

import ru.gostishcheva.tm.entity.Task;

import javax.ejb.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Singleton
@WebService
public class TaskEndpoint {
    private Map<String, Task> map = new LinkedHashMap<>();

    @WebMethod
    public Task merge(@WebParam(name = "task") Task task){
        map.put(task.getId(), task);
        return task;
    }
    @WebMethod
    public Task createTask(@WebParam(name = "name") String name){
        final Task task = new Task();
        task.setName(name);
        return merge(task);
    }
    @WebMethod
    public Collection<Task> findAll(){
        return map.values();
    }
    @WebMethod
    public Task findById(@WebParam(name = "id") String id){
        return map.get(id);
    }
    @WebMethod
    public void clear(){
        map.clear();
    }
}
