package ru.gostishcheva.tm.endpoint;

import ru.gostishcheva.tm.dao.ProjectDAO;
import ru.gostishcheva.tm.entity.Project;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Singleton
@WebService
public class ProjectEndpoint {

    @Inject
    private ProjectDAO projectDAO;

    @WebMethod
    public Project merge(@WebParam(name = "project") Project project){
        return projectDAO.merge(project);
    }
    @WebMethod
    public Project createProject(@WebParam(name = "name") String name){
        final Project project = new Project();
        project.setName(name);
        return projectDAO.merge(project);
    }
    @WebMethod
    public Collection<Project> findAll(){
        return projectDAO.findAll();
    }
    @WebMethod
    public Project findById(@WebParam(name = "id") String id){
        return projectDAO.findById(id);
    }
    @WebMethod
    public void clear(){
        projectDAO.clear();
    }
}
