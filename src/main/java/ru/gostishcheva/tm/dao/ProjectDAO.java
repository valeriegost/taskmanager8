package ru.gostishcheva.tm.dao;

import ru.gostishcheva.tm.entity.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class ProjectDAO {

    @PersistenceContext
    private EntityManager em;

    public Project persist(Project project){
        em.persist(project);
        return project;
    }

    public Project findById(String id){
        return em.find(Project.class, id);
    }

    public List<Project> findAll(){
        return em.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    public  Project merge(Project project){
        em.merge(project);
        return project;
    }

    public Project removeById(String id){
        final Project project = findById(id);
        if(project == null) return null;
        return remove(project);
    }

    public Project remove(Project project){
        em.remove(project);
        return project;
    }

    public void clear(){
        em.createQuery("DELETE FROM Project").executeUpdate();
    }

}
